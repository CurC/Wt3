<div id="rlcontent">
<div id="afield">
<div id="rlheader">
<h1 id="rlh1">Admin Menu</h1>
</div>
<?php echo form_open('admin/admin_form', 'id="rlform"'); ?>
	<input type="hidden" id='xfactorval' value="<?php echo $xfactorval; ?>">
	<input type="hidden" id='alfaval' value="<?php echo $alfaval; ?>">
	<div class="ainvul">
	<label class="alabel">X Factor (Afstand tussen persoonlijkshtypen en voorkeuren): </label>
	<input type="text" id="xfactor" readonly name='xfactor'>
	</div>
	<div id='xfactorslider'></div>
	<div class="ainvul">
	<label class="alabel">Alpha (Invloed van likes): </label>
	<input type="text" id="alfa" readonly name='alfa'>
	</div>
	<div id='alfaslider'></div>
	<div class="ainvul">
	<label class="alabel">Similarity measures: </label>
	<?php echo form_dropdown('merkberekening', array("1" => "Dice", "2" => "Jaccard", "3" => "Cosine", "4" => "Overlap"), $merkberekening, 'class="inputfield"'); ?>
	</div>
	<input type="submit" class="inputfield" value="Submit">
</form>
</div>
</div>