<div id="pcontent">
	<div id="pprofiel">
	<div class="pblok">
		<div id="pphoto">
			<?php $relatie = $gebruiker['relatietype'];
			if ($relatie == 1){
				$fotoid = "class = 'leftfoto'";
			}
			else if ($relatie == 2){
				$fotoid = "class = 'rightfoto'";
			}
			else if ($relatie == 3){
				$fotoid = "class = 'fullfoto'";
			}
			else{
				$fotoid = "class = 'nullfoto'";
			}
			?>
			<div <?php echo $fotoid;?>></div>
			<img class='foto' src="<?php echo base_url('assets/uploads/' . str_replace('_thumb', '', $gebruiker['foto']));?>">
		</div>
		<div id="pinfo">
			<?php echo $like_link ?>
			<div id="profielinform">
			<h1 id="naamhead"><?php echo $gebruiker['roepnaam'];?></h1>
			<p>Volledige naam: <?php echo $gebruiker['naam'];?></p>
			<p>Geslacht: <?php echo $gebruiker['geslacht'];?></p>
			<p>Geboortedatum: <?php echo $gebruiker['geboortedatum'];?></p>
			</div>
		</div>
	</div>
	<div class="pblok">
		<div id="pbeschrijving">
			<h1 class="ph1">Beschrijving:</h1>
			<p><?php echo $gebruiker['beschrijving'];?></p>
		</div>
		<div id="ppersoonlijkheid">
			<p>Persoonlijkheidstype: <?php echo $this->dbmodel->exists_mbti($gebruiker['id']) ? strtoupper($mbti->mbti) : "Heeft de test nog niet gemaakt";?></p>
			<p>Persoonlijkheidsvoorkeur: <?php echo $this->dbmodel->exists_mbti($gebruiker['id']) ? strtoupper($mbti->vk_mbti) : "Heeft de test nog niet gemaakt";?></p>
			<p>Leeftijdsvoorkeur: <?php echo $gebruiker['leeftijdsvk'];?></p>
			<p>Geslachtsvoorkeur: <?php echo $gebruiker['geslachtvk'];?></p>
		</div>
	</div>
	<div class="pblok">
		<div id="pmerken">
			<div class="ph1m"><h1 class="ph1">Merken die deze persoon leuk vind: </h1></div>
			<div class="merken">
			<?php foreach($merkvoorkeur as $merk)echo "<p>" . $merk->merknaam . "</p>";?>
			</div>
		</div>
	</div>
	</div>
</div>