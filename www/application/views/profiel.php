<div id="pcontent">
	<div id="pprofiel">
	<div class="pblok">
		<div id="pphoto">
			<img class='foto' src="<?php echo base_url('assets/uploads/' . str_replace('_thumb', '', $foto));?>">
		</div>
		<div id="pinfo">
			<a href="<?php echo base_url();?>index.php/profiel/edit">Edit Profiel</a>
			<?php echo $MBTI ? "" : "<a href=" . base_url('index.php/profiel/perstest') . ">Persoonlijkheidstest</a>"; ?>
			<div id="profielinform">
			<h1 id="naamhead"><?php echo $roepnaam;?></h1>
			<p>Volledige naam: <?php echo $naam;?></p>
			<p>Emailadres: <?php echo $email;?></p>
			<p>Geslacht: <?php echo $geslacht;?></p>
			<p>Geboortedatum: <?php echo $geboortedatum;?></p>
			</div>
		</div>
	</div>
	<div class="pblok">
		<div id="pbeschrijving">
			<h1 class="ph1">Beschrijving:</h1>
			<p><?php echo $beschrijving;?></p>
		</div>
		<div id="ppersoonlijkheid">
			<p>Persoonlijkheidstype: <?php echo $MBTI ? $MBTI : "De test is nog niet gemaakt"; ?></p>
			<p>Persoonlijkheidsvoorkeur: <?php echo $vk_MBTI ? $vk_MBTI : "De test is nog niet gemaakt"; ?></p>
			<p>Leeftijdsvoorkeur: <?php echo $leeftijdsvoorkeur;?></p>
			<p>Geslachtsvoorkeur: <?php echo $geslachtsvoorkeur;?></p>
		</div>
	</div>
	<div class="pblok">
		<div id="pmerken">
			<div class="ph1m"><h1 class="ph1">Merken die ik leuk vind: </h1></div>
			<div class="merken">
			<?php foreach($merkvoorkeur as $merk)echo "<p>" . $merk->merknaam . "</p>";?>
			</div>
		</div>
	</div>
	</div>
</div>