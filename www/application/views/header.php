<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/script.js"></script>
</head>
<body>
<nav>
	<ul>
        <li class="navhoofdkop">
            <ul>
            <li class="navkop">Overzicht</li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/browse/overzicht/1'); ?>">Liked</a>
            </li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/browse/overzicht/2'); ?>">Liked you</a>
            </li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/browse/overzicht/3'); ?>">Mutual</a>
            </li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/admin'); ?>">Admin</a>
            </li>
            </ul>
        </li>
        <li class="navhoofdkop">
            <ul>
            <li class="navkop">Profiel</li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/profiel'); ?>">Mijn Profiel</a>
            </li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/profiel/edit'); ?>">Edit Profiel</a>
            </li>
            </ul>
        </li>
        <li class="navhoofdkop">
            <ul>
            <li class="navkop">Browse</li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/browse'); ?>">Auto match</a>
            </li>
            <li class="navhoofdkop2 hidden">
                <a href="<?php echo base_url('index.php/browse/zoek'); ?>">Zoek</a>
            </li>
            </ul>
        </li>
        <li class="navhoofdkop">
            <ul>
            <?php $ingelogd = $this->session->userdata('logged_in'); ?>
            <li class="navkop"><?php echo $ingelogd ? "<a href=" . base_url('index.php/login/loguit') .  ">Log uit</a>" : 'Login' ?></li>
            <?php echo $ingelogd ? "" : "
            <li class='navhoofdkop2 hidden'>
                <a href=" . base_url('index.php/login') . ">Inloggen</a>
            </li>
            <li class='navhoofdkop2 hidden'>
                <a href=" . base_url('index.php/login/registratie') . ">Registreren</a>
            </li>"; ?>
            </ul>
        </li>
        <li class="navhoofdkop homepage">
            <ul>
            <li class="navkop"><a href="<?php echo base_url('index.php/home'); ?>">Datadate</a></li>
            </ul>
        </li>
	</ul>
</nav>

	<div id = "content">