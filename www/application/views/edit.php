<div id = "pcontent">
	<div id = "epprofiel">
		<div id="eprofiel">
		<h1>Verander uw profiel informatie</h1>
		<?php echo validation_errors(); ?>
		<?php echo $this->session->flashdata('error_foto') ?>
		<?php echo $this->session->flashdata('error_emailedit') ?>
		<?php echo form_open('profiel/editprofiel_form', 'id="epform"'); ?>
		<div>
		<input type="text" class="inputfield" name="roepnaam" value="<?php echo $this->session->userdata('roepnaam');?>">
		<input type="text" class="inputfield" name="naam" value="<?php echo $this->session->userdata('naam');?>">
		</div>
		<input type="text" class="inputfield" name="emailregistratie" value="<?php echo $this->session->userdata('email');?>">
		<div>
		<input type="radio" class="inputfield" name="geslacht" <?php if($this->session->userdata('geslacht') == "M")echo "checked='true'";?> value="M" <?php echo set_radio('geslacht', 'M'); ?>><label>Man</label>
		<input type="radio" class="inputfield" name="geslacht" <?php if($this->session->userdata('geslacht') == "V")echo "checked='true'";?> value="V" <?php echo set_radio('geslacht', 'V'); ?>><label>Vrouw</label>
		<label>Geslachtsvoorkeur:</label>
		<select class="inputfield" name="geslachtsvoorkeur">
			<option value="M" <?php if($this->session->userdata('geslachtsvoorkeur') == "M")echo "selected";?>>M</option>
			<option value="V" <?php if($this->session->userdata('geslachtsvoorkeur') == "V")echo "selected";?>>V</option>
			<option value="Beide" <?php if($this->session->userdata('geslachtsvoorkeur') == "Beide")echo "selected";?>>Beide</option>
		</select>
		</div>
		<div>
		<label>Geboortedatum: </label>
		<?php $gebdatum = explode("-", $this->session->userdata('geboortedatum'));?>
		<div id="date1" class="datefield inputfield">
    	<input id="day" class="inputfield" name="gebdatumd" type="text" maxlength="2" value="<?php echo $gebdatum[0];?>"/>/
    	<input id="month" class="inputfield" name="gebdatumm" type="text" maxlength="2"value="<?php echo $gebdatum[1];?>"/>/
    	<input id="year" class="inputfield" name="gebdatumy" type="text" maxlength="4" value="<?php echo $gebdatum[2];?>"/>
		</div>
		<label>Leeftijdsvoorkeur: </label>
		<input type="text" id="leeftijd" readonly name='leeftijdsvoorkeur'>
		<div id='leeftijdslider'></div>
		</div>
		<textarea rows="4" columns="50" class="inputfield" name="beschrijving"><?php echo $this->session->userdata('beschrijving');?></textarea>
		<?php echo $merken; ?>
		<input type="submit" class="inputfield" value="Submit">
		</form>
		</div>
		<div id="uploadfoto">
		<h1>Verander uw profielfoto</h1>
		<?php echo form_open_multipart('profiel/uploadfoto_form', 'id="epfotoform"'); ?>
		<input type="file" name="profielfoto">
		<input type="submit" value="Submit">
		</form>
		</div>
		<div id="ewachtwoord">
		<h1>Verander uw wachtwoord</h1>
		<?php echo form_open('profiel/editwachtwoord_form', 'id="ewform"'); ?>
		<label>Nieuw wachtwoord: </label>
		<input type="password" class="inputfield" name="pass">
		<input type="submit" class="inputfield" value="Submit">
		</form>
		</div>
		<div id="deleteprofiel">
		<input type="hidden" id="deletelink" value="<?php echo base_url('index.php/profiel/delete'); ?> ">
		<button class="inputfield">Delete profiel</button>
		<div id="delete-confirm" title="Delete Confirmation">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Uw profiel wordt permanent verwijderd. Bent U zeker?</p>
		</div>
		</div>
	</div>
</div>