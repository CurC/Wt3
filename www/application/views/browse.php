<div id="bcontent">
	<div id="bresults">
	<?php
	if (!$resultaat){
			echo "<div id='geenresult'><h1>We hebben geen gebruikers gevonden die passen bij dit criteria.
			 U kunt gebruikmaken van de <a href=" . base_url('index.php/browse/zoek') . ">zoek functie</a> om aangepaste criteria te gebruiken</h1></div>";
	}
		foreach($resultaat as $profiel){
		$id = $profiel['id'];
		$roepnaam = $profiel['roepnaam'];
		$geboortedatum = $profiel['geboortedatum'];
		$foto = $profiel['foto'];

		$geslacht = $profiel['geslacht'];
		$beschrijving = $profiel['beschrijving'];
		$posp = strpos($beschrijving, '.');
		$blength = strlen($beschrijving);
		$merken = "";
		foreach($profiel['merkvoorkeur'] as $merk)$merken .= $merk->merknaam . ", " ;
		$relatie = $profiel['relatietype'];
			if ($relatie == 1){
				$fotoid = "class = 'leftfotoh'";
			}
			else if ($relatie == 2){
				$fotoid = "class = 'rightfotoh'";
			}
			else if ($relatie == 3){
				$fotoid = "class = 'fullfotoh'";
			}
			else{
				$fotoid = "class = 'nullfoto'";
			}	
		if($posp == false && $blength > 75)
		{
			$beschrijvingFL = substr($beschrijving, 0, 75);
			$beschrijvingFL .= '...';
		}
		else if ($posp == false)
		{
			$beschrijvingFL = substr($beschrijving, 0, 75);
		}
		else
		{
			$beschrijvingFL = substr($beschrijving, 0, $posp+1);
		}
		
		echo '<div class="bprofiel">
				<div class="bfoto">
				<div ' . $fotoid . '></div>
				<a class="bfotolink" href="' . base_url() . 'index.php/browse/info/' . $id . '""><img class="foto" src="' . base_url('assets/uploads/' . $foto) . '"/></a></div>
				<div class="binfo"><a class="broepnaam" href ="' . base_url('index.php/browse/info/') . '/' . $id . '">' . $roepnaam . '</a>
								   <p>' . $geslacht . '</p>
								   <p>' . $geboortedatum . '</p>
								   <p>' . $beschrijvingFL . '</p>
								   <p>' . $merken . '</p>
								   </div>
			  </div>';
		}
	?>
	<div id="bpagelinks">
		<?php echo $pagelinks ?>
	</div>
	</div>
</div>
