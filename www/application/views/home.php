<div id="hall">
<div id="hcontent">
	<div id="hheader">
		<h1>Welkom op DataDate, de datingsite die u koppelt op een wetenschappelijke wijze!</h1>
		<?php echo $this->dbmodel->exists_like_wz(2,3); ?>
	</div>
	<?php $ingelogd = $this->session->userdata('logged_in'); ?>
	<?php echo !$ingelogd ? "
	<div id='hmain'>
			<h2>Registreer nu en vind uw perfecte match!</h2>
			<div id='hregistreer'>
			<a id='reglink' href='" . base_url("/index.php/login/registratie") . "'>
			<span id='hregistreerlink'>Klik hier om te registreren!</span>
			</a>
			</div>
	" : "<div id='hmain'";?>
	<p>De bijzonderheid in onze site is dat ons dating paradigma gebaseerd is
		 op een unieke en wetenschappelijk correct bewezen profilerings- en matching
		  techniek die zowel de persoonlijkheid als de lifestyle in de "dating equation"
		   meeneemt en leert van de voorkeuren van de gebruiker om de dating ervaring
		    te optimaliseren. Tot op zekere hoogte is dit al eens eerder vertoond
		     (BrandDating.nl voor dating op basis van lifestyle - deze site is kennelijk
		      wegen succes beëindigd - en Parship voor dating op basis van persoonlijkheid)
		      , maar wij hebben niet alleen betere technologieën; we combineren ook nog
		     eens deze systemen en breiden ze uit met "playing field changing" zelflerende functionaliteit.
		</p>
	<?php $MBTI = $this->session->userdata('MBTI'); ?>
	<?php echo $ingelogd ? ($MBTI  ? "<div id='perstest'><p>Uw heeft de persoonlijkheidstest al gemaakt,
	 ga gelijk op zoek naar uw <a href='" . base_url("index.php/browse/") . "'>matches</a></p></div>"
	  : "<div id='perstest'><p>U moet de <a href='" . base_url("index.php/profiel/perstest/")
	   . "'>persoonlijkheidstest</a> nog maken!</p>") : ""?>
	</div>
	<div id="hgebruikers">
		<div id="gebruikerheader"><h1>Onze leden</h1></div>
	<?php foreach($resultaat as $profiel){
		$id = $profiel['id'];
		$roepnaam = $profiel['roepnaam'];
		$geboortedatum = $profiel['geboortedatum'];
		$foto = $profiel['foto'];

		$geslacht = $profiel['geslacht'];
		$beschrijving = $profiel['beschrijving'];
		$posp = strpos($beschrijving, '.');
		$blength = strlen($beschrijving);
		$merken = "";
		foreach($profiel['merkvoorkeur'] as $merk)$merken .= $merk->merknaam . ", " ;

		$relatie = $profiel['relatietype'];
			if ($relatie == 1){
				$fotoid = "class = 'leftfotoh'";
			}
			else if ($relatie == 2){
				$fotoid = "class = 'rightfotoh'";
			}
			else if ($relatie == 3){
				$fotoid = "class = 'fullfotoh'";
			}
			else{
				$fotoid = "class = 'nullfoto'";
			}	

		if($posp == false && $blength > 75)
		{
			$beschrijvingFL = substr($beschrijving, 0, 75);
			$beschrijvingFL .= '...';
		}
		else if ($posp == false)
		{
			$beschrijvingFL = substr($beschrijving, 0, 75);
		}
		else
		{
			$beschrijvingFL = substr($beschrijving, 0, $posp+1);
		}
		
		echo '<div class="bprofiel">
				<div class="bfoto">
				<div ' . $fotoid . '></div>
				<a class="bfotolink" href="' . base_url() . 'index.php/browse/info/' . $id . '"><img class="foto" alt="profielfoto" src="' . base_url('assets/uploads/' . $foto) . '"/></a></div>
				<div class="binfo"><a class="broepnaam" href ="' . base_url('index.php/browse/info/') . '/' . $id . '">' . $roepnaam . '</a>
								   <p>' . $geslacht . '</p>
								   <p>' . $geboortedatum . '</p>
								   <p>' . $beschrijvingFL . '</p>
								   <p>' . $merken . '</p>
								   </div>
			  </div>';
		}
	?>
	</div>
	<div>
	<a href="<?php echo base_url('index.php/home/');?>" id="meerrandom"><span id="meerladen">Meer Random!</span></a>
	</div>
</div>
</div>