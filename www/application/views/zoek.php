<div id="rlcontent">
<div id="column">
<?php echo validation_errors(); ?>
<?php echo form_open('browse/zoek_resultaten', 'id="zoek"'); ?>
	<div id="zoekk">
	<div id="zoekr">
	<div><input type="radio" class="inputfield" name="EI" id="E" <?php echo set_radio('EI', '100'); ?> value="100"><label for="E">Extroversion</label></div>
	<div><input type="radio" class="inputfield" name="EI" id="I" <?php echo set_radio('EI', '0'); ?> value="0" ><label for="I">Introversion</label></div>
	</div>
	<div>
	<div><input type="radio" class="inputfield" name="NS" id="N" <?php echo set_radio('NS', '100'); ?> value="100"><label for="N">Intuition</label></div>
	<div><input type="radio" class="inputfield" name="NS" id="S" <?php echo set_radio('NS', '0'); ?> value="0" ><label for="S">Sensing</label></div>
	</div>
	<div>
	<div><input type="radio" class="inputfield" name="TF" id="T" <?php echo set_radio('TF', '100'); ?> value="100"><label for="T">Thinking</label></div>
	<div><input type="radio" class="inputfield" name="TF" id="F" <?php echo set_radio('TF', '0'); ?> value="0" ><label for="F">Feeling</label></div>
	</div>
	<div>
	<div><input type="radio" class="inputfield" name="JP" id="J" <?php echo set_radio('JP', '100'); ?> value="100"><label for="J">Judging</label></div>
	<div><input type="radio" class="inputfield" name="JP" id="P" <?php echo set_radio('JP', '0'); ?> value="0" ><label for="P">Perceiving</label></div>
	</div>
	</div>
	<label>Geslachtsvoorkeur:</label>
	<select class="inputfield" name="geslachtsvoorkeur">
		<option value="M" <?php echo set_select('geslachtsvoorkeur', 'M'); ?>>M</option>
		<option value="V" <?php echo set_select('geslachtsvoorkeur', 'V'); ?>>V</option>
		<option value="Beide" <?php echo set_select('geslachtsvoorkeur', 'Beide'); ?>>Beide</option>
	</select>
	<label for="leeftijd">Leeftijdsvoorkeur:</label>
	<input type="text" id="leeftijd" readonly name='leeftijdsvoorkeur'>
	<div id='leeftijdslider'></div>
	Merkvoorkeurtjes
	<?php echo $merken; ?>
	<input type="submit" class="inputfield" value="Submit">
</form>
</div>
</div>