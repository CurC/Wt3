<div id="rlcontent">
<div id="rlfield">
<div id="rlheader">
<h1 id="rlh1">Registratie</h1>
</div>
<?php echo validation_errors(); ?>
<?php echo form_open('login/check_registratieform', 'id="rlform"'); ?>
	<div>
	<input type="text" class="inputfield" name="roepnaam" value="<?php echo set_value('roepnaam'); ?>" placeholder="Roepnaam">
	<input type="text" class="inputfield" name="naam" value="<?php echo set_value('naam'); ?>" placeholder="Naam">
	</div>
	<input type="text" class="inputfield" name="emailregistratie" value="<?php echo set_value('emailregistratie'); ?>" placeholder="Email">
	<input type="password" class="inputfield" name="pass" value="<?php echo set_value(''); ?>" placeholder="Wachtwoord">
	<input type="password" class="inputfield" name="passconf" value="<?php echo set_value(''); ?>" placeholder="Herhaal uw wachtwoord">
	<label>Geslacht:</label>
	<div><input type="radio" class="inputfield" name="geslacht" value="M" <?php echo set_radio('geslacht', 'M'); ?>><label>Man</label></div>
	<div><input type="radio" class="inputfield" name="geslacht" value="V" <?php echo set_radio('geslacht', 'V'); ?>><label>Vrouw</label></div>
	<label>Geslachtsvoorkeur:</label>
	<select class="inputfield" name="geslachtsvoorkeur">
		<option value="M" <?php echo set_select('geslachtsvoorkeur', 'M'); ?>>M</option>
		<option value="V" <?php echo set_select('geslachtsvoorkeur', 'V'); ?>>V</option>
		<option value="Beide" <?php echo set_select('geslachtsvoorkeur', 'Beide'); ?>>Beide</option>
	</select>
	<label>Geboortedatum: </label>
	<div id="date1" class="datefield inputfield">
    <input id="day" class="inputfield" name="gebdatumd" type="text" maxlength="2" placeholder="DD" value="<?php echo set_value('gebdatumd'); ?>"/>-
    <input id="month" class="inputfield" name="gebdatumm" type="text" maxlength="2" placeholder="MM" value="<?php echo set_value('gebdatumm'); ?>"/>-
    <input id="year" class="inputfield" name="gebdatumy" type="text" maxlength="4" placeholder="YYYY" value="<?php echo set_value('gebdatumy'); ?>"/>
	</div>
	<label>Leeftijdsvoorkeur: </label>
	<input type="text" id="leeftijd" readonly name='leeftijdsvoorkeur'>
	<div id='leeftijdslider'></div>
	<label>Vertel wat leuks over jezelf: </label>
	<textarea rows="4" columns="50" class="inputfield" name="beschrijving"><?php echo set_value('beschrijving'); ?></textarea>
	Merkvoorkeurtjes
	<?php echo $merken; ?>
	<input type="submit" class="inputfield" value="Submit">
</form>
</div>
</div>