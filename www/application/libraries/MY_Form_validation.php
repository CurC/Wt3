<?php

class MY_Form_validation extends CI_Form_validation
{
	protected $CI;

	function __construct($config = array())
	{
		parent::__construct($config);
		$this->CI =& get_instance();
	}

	function date_valid ($str)
	{
		return checkdate((int) $this->CI->input->post('gebdatumm'), 
			(int) $this->CI->input->post('gebdatumd'), 
			(int) $this->CI->input->post('gebdatumy'));
	}

	function ouder_dan_18 ($str)
	{
		$geboortedatum = strtotime((int) $this->CI->input->post('gebdatumm') . '-' .
			(int) $this->CI->input->post('gebdatumd') . '-' .
			(int) $this->CI->input->post('gebdatumy'));
		$vandaag = time();
		return strtotime('-18 years', $vandaag) > $geboortedatum;
	}
}