<?php

class Login extends CI_Controller
{

	public function index()
	{
		if ($this->session->userdata('logged_in')) 
		{
			redirect('home');
		}
		$data['title'] = 'Login';
		$this->load->view('header', $data);
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function registratie()
	{
		$this->prep_registratie($data);
		$data['title'] = 'Registratie';
		$this->load->view('header', $data);
		$this->load->view('registratie');
		$this->load->view('footer');
	}

	public function loguit()
	{
		$this->session->sess_destroy();
		redirect('/home');	
	}

	// Bereidt de registratie form voor
	private function prep_registratie(&$data)
	{
		$merkendata = $this->dbmodel->get_merken();
		$data['merken'] = "<div id='merkregistratie'><h3>Merkvoorkeur</h3><div id='merken'>";
		foreach ($merkendata as $row)
		{
			$data['merken'] .= "<div><input type='checkbox' name='merken[]' 
				value=" . $row->mid . " ><label>" . $row->merknaam . "</label></div>";
		}
		$data['merken'] .= "</div></div>";
	}

	public function check_loginform()
	{
		if ($this->form_validation->run('login') == false)
		{
			$data['title'] = 'Login';
			$this->load->view('header', $data);
			$this->load->view('login');
			$this->load->view('footer');
		}
		else
		{
			$this->check_gebruiker(
				$this->input->post('email'), 
				md5($this->input->post('pass'))
			);
		}
	}

	// Checkt de inloggegevens van de gebruiker
	private function check_gebruiker($email, $pass)
	{
		$resultaat = $this->dbmodel->get_gebruiker($email, $pass);
		if ($resultaat->email === $email && $resultaat->wachtwoord === $pass)
		{
			$silhouette = $resultaat->geslacht == 'M' ? 'man.png' : 'vrouw.png';

			$this->session->set_userdata(array(
				'id' => $resultaat->id,
				'email' => $resultaat->email,
				'roepnaam' => $resultaat->roepnaam,
				'naam' => $resultaat->naam,
				'geslacht' => $resultaat->geslacht,
				'geboortedatum' => $resultaat->geboortedatum,
				'foto' => $resultaat->foto ? $resultaat->foto : $silhouette,
				'beschrijving' => $resultaat->beschrijving,
				'geslachtsvoorkeur' => $resultaat->geslachtvk,
				'leeftijdsvoorkeur' => $resultaat->leeftijdsvk,
				'merkvoorkeur' => $resultaat->merkvk,
				'logged_in' => TRUE,
				'admin' => $resultaat->admin
			));

			if ($this->dbmodel->exists_mbti($resultaat->id))
			{
				$mbti = $this->dbmodel->get_mbti($resultaat->id);
				$this->session->set_userdata(array(
					'EI' => $mbti->ei,
					'NS' => $mbti->ns,
					'TF' => $mbti->tf,
					'JP' => $mbti->jp,
					'vk_EI' => $mbti->vk_ei,
					'vk_NS' => $mbti->vk_ns,
					'vk_TF' => $mbti->vk_tf,
					'vk_JP' => $mbti->vk_jp,
					'MBTI' => strtoupper($mbti->mbti),
					'vk_MBTI' => strtoupper($mbti->vk_mbti)
				));
			}

			redirect('/home');
		}
		else
		{
			$this->session->set_flashdata('login_error', 'Verkeerd email en wachtwoord combinatie');
			redirect('/login');
		}
	}

	// Verwerkt de registratie van een gebruiker
	public function check_registratieform()
	{
		if ($this->form_validation->run('registratie') == FALSE)
		{
			$this->prep_registratie($data);
			$data['title'] = 'Registratie';
			$this->load->view('header', $data);
			$this->load->view('registratie', $data);
			$this->load->view('footer');
		}
		else
		{
			$geboortedatum = $this->input->post('gebdatumd') . "-" . $this->input->post('gebdatumm') . '-' . $this->input->post('gebdatumy');
			$this->dbmodel->insert_gebruiker(
				$this->input->post('roepnaam'),
				$this->input->post('naam'),
				$this->input->post('emailregistratie'),
				md5($this->input->post('pass')),
				$this->input->post('geslacht'),
				$this->input->post('geslachtsvoorkeur'),
				$geboortedatum,
				$this->input->post('leeftijdsvoorkeur'),
				$this->input->post('beschrijving')
			);

			$gebruiker = $this->dbmodel->get_gebruiker($this->input->post('emailregistratie'),
				md5($this->input->post('pass')));
			$merkstring = '';
			foreach ($this->input->post('merken') as $row)
			{
				// $this->dbmodel->insert_merkvoorkeur($gebruiker->id, $row);
				$merkstring .= $row . ' ';
			}

			$this->dbmodel->update_merkvoorkeur($gebruiker->id, trim($merkstring));
			$this->check_gebruiker($this->input->post('emailregistratie'), md5($this->input->post('pass')));
		}
	}
}
