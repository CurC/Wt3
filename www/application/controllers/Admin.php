<?php

class Admin extends MY_AUTH {

	/*public function index()
	{
		$data['title'] = 'Overzicht';
		$gebruikers = $this->dbmodel->get_mbti_browse();
		$this->verwerk_overzicht($gebruikers);
		$this->load->view('header', $data);
		$this->load->view('overzicht', $data);
		$this->load->view('footer');
	}*/

	public function index()
	{
		$this->check_admin();
		$data['title'] = 'Admin menu';
		$data['xfactorval'] = $this->dbmodel->get_adminvar('x_factor');
		$data['alfaval'] = $this->dbmodel->get_adminvar('alpha');
		$data['merkberekening'] = $this->dbmodel->get_adminvar('merkberekening');
		$this->load->view('header.php', $data);
		$this->load->view('admin.php', $data);
		$this->load->view('footer.php', $data);
	}

	public function admin_form()
	{
		$this->dbmodel->update_adminvar('x_factor', $this->input->post('xfactor'));
		$this->dbmodel->update_adminvar('alpha', $this->input->post('alfa'));
		$this->dbmodel->update_adminvar('merkberekening', $this->input->post('merkberekening'));
		redirect('admin');
	}

	public function check_admin()
	{
		if ($this->session->userdata('admin'))
		{
			return;
		}
		else
		{
			redirect('home');
		}
	}
}