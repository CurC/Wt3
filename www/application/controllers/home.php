<?php

class Home extends MY_Controller {

	public function index()
	{
		$data['title'] = 'Home';
		$gebruikers = $this->dbmodel->get_gebruiker_random();

		foreach ($gebruikers as &$row) 
		{
			$limit = 10;
			$merkinput = explode(' ', $row['merkvk']);
			shuffle($merkinput);
			$merkoutput = implode(' ', $merkinput);
			$merkvoorkeuren = strlen($merkoutput) > $limit ? substr($merkoutput, 0, strrpos(substr($merkoutput, 0, $limit), ' ')) : $merkoutput;
			$this->set_sillhouette($row['foto'], $row['geslacht']);
			$this->verwerk_merken_info($row, $merkvoorkeuren);
			$this->verwerk_relatietype($row);
		}

		$data['resultaat'] = $gebruikers;
		$this->is_ajax_request($data, 'home');
	}
}