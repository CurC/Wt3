<?php

class Browse extends MY_Controller {

	public function index()
	{
		$this->match();
	}

	// Profiel detail pagina
	public function info()
	{
		$gebruikerid = $this->uri->segment(3);

		if ($this->session->userdata('id') === $gebruikerid)
		{
			redirect('profiel');
		}

		$gebruiker = $this->dbmodel->get_gebruiker_info_array($gebruikerid);
		$mbti = $this->dbmodel->get_mbti($gebruikerid);
		$this->set_sillhouette($gebruiker['foto'], $gebruiker['geslacht']);
		$data['like_link'] = '';

		// Merkvoorkeur
		$this->verwerk_merken_info($data, $gebruiker['merkvk']);

		// Like voorwaarden
		if ($this->session->userdata('logged_in') && $this->dbmodel->exists_mbti($gebruikerid))
		{
			if ($this->dbmodel->exists_mbti($this->session->userdata('id')))
			{
				if (!$this->dbmodel->exists_like($this->session->userdata('id'), $gebruikerid))
				{
					$like_url = base_url('/index.php/browse/like/' . $gebruikerid);
					$data['like_link'] = '<a href=' . $like_url . '>Like</a>';
				}
				else
				{
					$data['like_link'] = '<a href="">Deze persoon heb je al geliked</a>';
				}
			}
			else
			{
				$data['like_link'] = '<a href="">Je moet het persoonlijkheids test hebben gemaakt om te kunnen liken</a>';		
			}
		}

		$this->verwerk_relatietype($gebruiker);
		$data['title'] = 'Profiel van ' . $gebruiker['roepnaam'];
		$data['gebruiker'] = $gebruiker;
		$data['mbti'] = $mbti;
		$this->load->view('header', $data);
		$this->load->view('browseprofiel', $data);
		$this->load->view('footer');
	}

	public function like()
	{
		$id = $this->session->userdata('id');
		$id2 = $this->uri->segment(3);

		if ($id === $id2)
		{
			redirect('home');
		}

		$alpha = $this->dbmodel->get_adminvar('alpha');
		if ((!$this->dbmodel->exists_like($id, $id2)) && $this->dbmodel->exists_mbti($id) && $this->dbmodel->exists_mbti($id2))
		{
			$this->dbmodel->insert_like($id, $id2);
			$mbti = $this->dbmodel->get_mbti($id);
			$mbti2 = $this->dbmodel->get_mbti($id2);
			$mbti->vk_ei = ($alpha * $mbti->vk_ei) + ((1 - $alpha) * $mbti2->ei);
			$mbti->vk_ns = ($alpha * $mbti->vk_ns) + ((1 - $alpha) * $mbti2->ns);
			$mbti->vk_tf = ($alpha * $mbti->vk_tf) + ((1 - $alpha) * $mbti2->tf);
			$mbti->vk_jp = ($alpha * $mbti->vk_jp) + ((1 - $alpha) * $mbti2->jp);
			$this->dbmodel->update_mbti_voorkeur($id, $mbti->vk_ei, $mbti->vk_ns, $mbti->vk_tf, $mbti->vk_jp);
		}
		redirect('browse/info/' . $id2);
	}

	// Vertoont gebruikers in een bepaalde volgorde en handelt ajax requests
	public function match()
	{
		$data['title'] = 'Browse';
		$item = $this->uri->segment((($this->uri->total_segments() > 3) ? $this->uri->total_segments() : 3), 0);
		$per_page = 6;
		$gebruikers = $this->dbmodel->get_mbti_browse();

		if($this->session->userdata('logged_in') && $this->dbmodel->exists_mbti($this->session->userdata('id')))
		{
			$mbti = $this->dbmodel->get_mbti($this->session->userdata('id'));
			$this->filter_linksvoorkeur($gebruikers, $this->session->userdata('geboortedatum'), $this->session->userdata('geslacht'));
			$this->filter_rechtsvoorkeur($gebruikers, $this->session->userdata('leeftijdsvoorkeur'), $this->session->userdata('geslachtsvoorkeur'));
			$this->verwerk_gebruikers($gebruikers,
					array(
						'EI' => $mbti->ei, 
						'NS' => $mbti->ns, 
						'TF' => $mbti->tf, 
						'JP' => $mbti->jp,
						'vk_EI' => $mbti->vk_ei,
						'vk_NS' => $mbti->vk_ns,
						'vk_TF' => $mbti->vk_tf,
						'vk_JP' => $mbti->vk_jp,
					),
					explode(' ', $this->session->userdata('merkvoorkeur')));
		}
		else
		{
			redirect('browse/zoek');
		}

		$data['resultaat'] = array_slice($gebruikers, $item, $per_page);
		$this->prep_paginering($data, 'browse/match', count($gebruikers), 3);
		$this->is_ajax_request($data, 'browse');
	}

	// Overzicht van de wie wie heeft geliked
	public function overzicht()
	{
		$data['title'] = 'Overzicht';
		$type = $this->uri->segment(3) >= 1 ? $this->uri->segment(3) : 1;
		$item = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		$per_page = 6;
		$gebruikers = $this->dbmodel->get_mbti_browse();

		if($this->session->userdata('logged_in') && $this->dbmodel->exists_mbti($this->session->userdata('id')))
		{
			$mbti = $this->dbmodel->get_mbti($this->session->userdata('id'));
			$this->verwerk_overzicht($gebruikers);
			$this->type_overzicht($gebruikers, $type);
			$this->verwerk_gebruikers($gebruikers,
					array(
						'EI' => $mbti->ei, 
						'NS' => $mbti->ns, 
						'TF' => $mbti->tf, 
						'JP' => $mbti->jp,
						'vk_EI' => $mbti->vk_ei,
						'vk_NS' => $mbti->vk_ns,
						'vk_TF' => $mbti->vk_tf,
						'vk_JP' => $mbti->vk_jp,
					),
					explode(' ', $this->session->userdata('merkvoorkeur')));
		}
		else
		{
			redirect('login');
		}

		$data['resultaat'] = array_slice($gebruikers, $item, $per_page);
		$this->prep_paginering($data, 'browse/overzicht/' . $type, count($gebruikers), 4);
		$this->is_ajax_request($data, 'browse');
	}

	private function verwerk_overzicht(&$gebruikers)
	{
		foreach ($gebruikers as &$row) 
		{
			$this->verwerk_relatietype($row);
		}
	}

	// Filtert op type overzicht
	private function type_overzicht(&$gebruikers, $type)
	{
		foreach($gebruikers as $key => $row)
		{
			if ($row['relatietype'] != $type) {
				unset($gebruikers[$key]);
			}
		}
	}

	// Zoek invulpagina
	public function zoek()
	{
		$this->prep_zoek_form($data);
		$data['title'] = 'Zoeken';
		$this->load->view('header', $data);
		$this->load->view('zoek', $data);
		$this->load->view('footer');
	}

	private function prep_zoek_form(&$data)
	{
		$merkendata = $this->dbmodel->get_merken();
		$data['merken'] = "<div id='merkregistratie'><h3>Merkvoorkeur</h3><div id='merkenb'>";
		foreach ($merkendata as $row)
		{
			$data['merken'] .= "<div><input type='checkbox' name='merken[]' 
				value=" . $row->mid . " ><label>" . $row->merknaam . "</label></div>";
		}
		$data['merken'] .= "</div></div>";		
	}

	public function zoek_resultaten()
	{
		if ($this->form_validation->run('zoek') == FALSE)
		{
			$data['title'] = 'Zoeken';
			$this->prep_zoek_form($data);
			$this->load->view('header', $data);
			$this->load->view('zoek', $data);
			$this->load->view('footer');
		}
		else
		{
			$this->session->zoek = array(
					'EI' => $this->input->post('EI'),
					'NS' => $this->input->post('NS'),
					'TF' => $this->input->post('TF'),
					'JP' => $this->input->post('JP'),
					'geslachtsvoorkeur' => $this->input->post('geslachtsvoorkeur'),
					'leeftijdsvoorkeur' => $this->input->post('leeftijdsvoorkeur'),
					'merken' => $this->input->post('merken')
				);
			$this->match_zoek();
		}
	}

	public function match_zoek()
	{
		$data['title'] = 'Browse';
		$item = $this->uri->segment((($this->uri->total_segments() > 3) ? $this->uri->total_segments() : 3), 0);
		$per_page = 6;
		$gebruikers = $this->dbmodel->get_mbti_browse();
		$leeftijdsvoorkeur = $this->session->zoek['leeftijdsvoorkeur'];
		$geslachtsvoorkeur = $this->session->zoek['geslachtsvoorkeur'];

		$this->filter_rechtsvoorkeur($gebruikers, $leeftijdsvoorkeur, $geslachtsvoorkeur);
		$this->verwerk_gebruikers($gebruikers,
				array(
					'EI' => 100 - $this->session->zoek['EI'],
					'NS' => 100 - $this->session->zoek['NS'],
					'TF' => 100 - $this->session->zoek['TF'],
					'JP' => 100 - $this->session->zoek['JP'],
					'vk_EI' => $this->session->zoek['EI'],
					'vk_NS' => $this->session->zoek['NS'],
					'vk_TF' => $this->session->zoek['TF'],
					'vk_JP' => $this->session->zoek['JP'],
				),
				$this->session->zoek['merken']);

		$data['resultaat'] = array_slice($gebruikers, $item, $per_page);
		$this->prep_paginering($data, 'browse/match_zoek', count($gebruikers), 3);	
		$this->is_ajax_request($data, 'browse');
	}

	// Haalt de gebruikers op voor de zoek functie
	private function verwerk_gebruikers(&$gebruikers, $mbti_array, $merken)
	{
		$x_factor = $this->dbmodel->get_adminvar('x_factor');
		$match_coeff = array();
		
		foreach ($gebruikers as $key => &$row)
		{

			// Haalt de gebruiker zelf uit de pool
			if ($row['id'] == $this->session->userdata('id'))
			{
				unset($gebruikers[$key]);
				continue;
			}

			// Persoonlijkheidstype
			$row['pres_coeff'] = max(1 - (ABS($mbti_array['EI'] - $row['vk_ei']) + 
							ABS($mbti_array['NS'] - $row['vk_ns']) + 
							ABS($mbti_array['TF'] - $row['vk_tf']) + 
							ABS($mbti_array['JP'] - $row['vk_jp']))/400.0,
							1 - (ABS($row['ei'] - $mbti_array['vk_EI']) + 
							ABS($row['ns'] - $mbti_array['vk_NS']) + 
							ABS($row['tf'] - $mbti_array['vk_TF']) + 
							ABS($row['jp'] - $mbti_array['vk_JP']))/400.0
						);

			$pres_coeff[$key] = $row['pres_coeff'];

			// Geeft elke gebruiker een relatietype
			$this->verwerk_relatietype($row);

			// Verwerk merkvoorkeur
			$limit = 10;
			$merkinput = explode(' ', $row['merkvk']);
			shuffle($merkinput);
			$merkoutput = implode(' ', $merkinput);
			$merkvoorkeuren = strlen($merkoutput) > $limit ? substr($merkoutput, 0, strrpos(substr($merkoutput, 0, $limit), ' ')) : $merkoutput;
			$this->verwerk_merken_info($row, $merkvoorkeuren);

			// Silhouette als er geen foto is opgeslagen
			$this->set_sillhouette($row['foto'], $row['geslacht']);

			// Merkencoeeficient
			$this->verwerk_gebruikers_merk($row, $merken, $this->dbmodel->get_adminvar('merkberekening'));

			$row['match_coeff'] = ($x_factor*$row['pres_coeff']) + ((1-$x_factor)*$row['merk_coeff']);
			$match_coeff[$key] = $row['match_coeff'];
		}

		array_multisort($match_coeff, SORT_DESC, $gebruikers);
	}

	public function verwerk_gebruikers_merk(&$gebruiker, $merken, $type)
	{
		switch ($type) {
			case 1:
				$gebruiker['merk_coeff'] = (float) (2 * count(array_intersect($merken, explode(' ', $gebruiker['merkvk']))) / 
					(count($merken) + count(explode(' ', $gebruiker['merkvk']))));
				break;
			case 2:
				$gebruiker['merk_coeff'] = (float) (count(array_intersect($merken, explode(' ', $gebruiker['merkvk']))) / 
					count(array_unique(array_merge($merken, explode(' ', $gebruiker['merkvk'])))));
				break;
			case 3:
				$gebruiker['merk_coeff'] = (float) (count(array_intersect($merken, explode(' ', $gebruiker['merkvk']))) / 
					(sqrt(count($merken)) * sqrt(count(explode(' ', $gebruiker['merkvk'])))));
				break;
			case 4:
				$gebruiker['merk_coeff'] = (float) (count(array_intersect($merken, explode(' ', $gebruiker['merkvk']))) / 
					(min(count($merken), count(explode(' ', $gebruiker['merkvk'])))));
				break;
		}
	}

	// Filter resultaten die niet in overstemming zijn met de voorkeuren van de gebruiker
	private function filter_rechtsvoorkeur(&$gebruikers, $leeftijdsvoorkeur, $geslachtsvoorkeur)
	{
		foreach ($gebruikers as $key => &$row)
		{
			// Leeftijdsvoorkeur
			$leeftijdsvoorkeur_array = explode('-', $leeftijdsvoorkeur);
			$row['leeftijd'] = floor((time() - strtotime($row['geboortedatum'])) / 31557600);
			if ($row['leeftijd'] < $leeftijdsvoorkeur_array[0] || $row['leeftijd'] > $leeftijdsvoorkeur_array[1])
			{
				unset($gebruikers[$key]);
				continue;
			}

			// Geslachtsvoorkeur
			if ($geslachtsvoorkeur !== "Beide")
			{
				if ($geslachtsvoorkeur !== $row['geslacht'])
				{
					unset($gebruikers[$key]);
					continue;
				}
			}
		}
	}

	// Filter resultaten die niet in overstemming zijn met de voorkeuren van het resultaat
	private function filter_linksvoorkeur(&$gebruikers, $geboortedatum, $geslacht)
	{
		foreach ($gebruikers as $key => $row)
		{
			// Leeftijdsvoorkeur
			$leeftijdsvoorkeur = explode('-', $row['leeftijdsvk']);
			$leeftijd = floor((time() - strtotime($geboortedatum)) / 31557600);
			if ($leeftijd < $leeftijdsvoorkeur[0] || $leeftijd > $leeftijdsvoorkeur[1])
			{
				unset($gebruikers[$key]);
				continue;
			}

			// Geslachtsvoorkeur	
			if ($row['geslachtvk'] !== "Beide")
			{
				if ($row['geslachtvk'] !== $geslacht)
				{
					unset($gebruikers[$key]);
					continue;
				}
			}
		}
	}

	// Paginering
	private function prep_paginering(&$data, $functie, $totaal, $segment)
	{
		$config['base_url'] = base_url("index.php/" . $functie);
		$config['total_rows'] = $totaal;
    	$config['uri_segment']  = $segment;
		$config['per_page'] = 6;
		$config['attributes'] = array('class' => 'page');
		$this->pagination->initialize($config);
		$data['pagelinks'] = $this->pagination->create_links();
	}

}
