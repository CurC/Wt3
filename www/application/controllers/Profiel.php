<?php

class Profiel extends MY_AUTH {

	public function index()
	{
		$data['title'] = 'Mijn profiel';
		$this->prep_gebruikersprofiel($data);
		$this->load->view('header', $data);
		$this->load->view('profiel', $data);
		$this->load->view('footer');
	}

	private function prep_gebruikersprofiel(&$data)
	{
		$velden = array('roepnaam', 'naam', 'email', 'geslacht', 'geboortedatum', 'beschrijving', 'geslachtsvoorkeur', 'leeftijdsvoorkeur', 'foto', 'MBTI', 'vk_MBTI');
		foreach ($velden as $veld)
		{
			$data["$veld"] = $this->session->userdata("$veld");
		}

		// Merkvoorkeur
		$this->verwerk_merken_info($data, $this->session->userdata('merkvoorkeur'));
	}

	public function edit()
	{
		$data['title'] = 'Mijn profiel';
		$this->prep_gebruikersprofiel($data);
		$this->prep_profieledit($data);
		$this->load->view('header', $data);
		$this->load->view('edit', $data);
		$this->load->view('footer');
	}

	public function delete()
	{
		$id = $this->session->userdata('id');
		$this->dbmodel->delete_gebruiker($id);
		$this->dbmodel->delete_mbti($id);
		$this->dbmodel->delete_likes($id);
		$this->dbmodel->delete_merkvoorkeur($id);
		redirect('login/loguit');
	}

	public function editprofiel_form()
	{
		// Check of de ingevulde email hetzelfde is, of als het een nieuwe is, dat die niet bestaat in de database
		$emailvalid = $this->input->post('emailregistratie') == $this->session->userdata('email') || 
				($this->input->post('emailregistratie') != $this->session->userdata('email') && 
							!$this->dbmodel->exists_email($this->input->post('emailregistratie')));
		
		if ($this->form_validation->run('edit_profiel') == FALSE || !$emailvalid)
		{
			$data['title'] = 'Edit profiel';
			$emailvalid ? $this->session->set_flashdata('error_emailedit', '') : $this->session->set_flashdata('error_emailedit', 'U moet een unique email kiezen');
			$this->prep_gebruikersprofiel($data);
			$this->prep_profieledit($data);
			$this->load->view('header', $data);
			$this->load->view('edit', $data);
			$this->load->view('footer');
		}
		else
		{
			$geboortedatum = $this->input->post('gebdatumd') . "-" . $this->input->post('gebdatumm') . '-' . $this->input->post('gebdatumy');
			$this->dbmodel->update_gebruikersprofiel(
				$this->session->userdata('id'),
				$this->input->post('roepnaam'),
				$this->input->post('naam'),
				$this->input->post('emailregistratie'),
				$this->input->post('geslacht'),
				$this->input->post('geslachtsvoorkeur'),
				$geboortedatum,
				$this->input->post('leeftijdsvoorkeur'),
				$this->input->post('beschrijving')
			);

			// Update merkvoorkeuren
			$this->dbmodel->delete_merkvoorkeur($this->session->userdata('id'));
			$merkstring = '';
			foreach ($this->input->post('merken') as $row)
			{
				// $this->dbmodel->insert_merkvoorkeur($this->session->userdata('id'), $row);
				$merkstring .= $row . ' ';
			}

			$this->dbmodel->update_merkvoorkeur($this->session->userdata('id'), trim($merkstring));

			$this->session->set_userdata(array(
				'email' => $this->input->post('emailregistratie'),
				'roepnaam' => $this->input->post('roepnaam'),
				'naam' => $this->input->post('naam'),
				'geslacht' => $this->input->post('geslacht'),
				'geboortedatum' => $geboortedatum,
				'beschrijving' => $this->input->post('beschrijving'),
				'geslachtsvoorkeur' => $this->input->post('geslachtsvoorkeur'),
				'leeftijdsvoorkeur' => $this->input->post('leeftijdsvoorkeur')
			));
			redirect('/profiel');
		}
	}

	public function uploadfoto_form()
	{
		$config['upload_path'] = './assets/uploads';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = 128;
		$config['max_width'] = 512;
		$config['max_height'] = 512;
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('profielfoto'))
		{
			$this->session->set_flashdata('error_foto', $this->upload->display_errors());
			$this->edit();
		}
		else
		{
			$fotopath = $this->upload->data('file_path');
			$fotonaam = $this->upload->data('file_name');
			$fotonaam_thumb = $this->upload->data('raw_name') . '_thumb' . $this->upload->data('file_ext');
			$this->dbmodel->update_gebruikersfoto($this->session->userdata('id'), $fotonaam_thumb);
			$this->session->set_userdata(array(
				'foto' => $fotonaam_thumb));
			
			$config['image_library'] = 'gd2';
			$config['source_image'] = $fotopath . $fotonaam;
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 128;
			$config['height']       = 128;

			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			
			$this->index();
		}
	}

	public function editwachtwoord_form()
	{
		if ($this->form_validation->run('edit_wachtwoord') == FALSE)
		{
			$data['title'] = 'Edit profiel';
			$this->load->view('header');
			$this->load->view('edit');
			$this->load->view('footer');
		}
		else
		{
			$this->dbmodel->update_gebruikerswachtwoord(
				$this->session->userdata('id'),
				md5($this->input->post('pass'))
			);
		}
	}

	private function prep_profieledit(&$data)
	{
		$merkendata = $this->dbmodel->get_merken();
		$merkvoorkeur_gesaved = explode(' ', $this->session->userdata('merkvoorkeur'));
		$data['merken'] = "<div id='merkregistratie' class='merkedit'><h3>Merkvoorkeur</h3><div id='merken'>";
		foreach ($merkendata as $row)
		{
			$data['merken'] .= "<div><input type='checkbox' name='merken[]'" . (in_array($row->mid, $merkvoorkeur_gesaved, true) ? "checked='true' " : " ") .
				"value=" . $row->mid . " ><label>" . $row->merknaam . "</label></div>";
		}
		$data['merken'] .= "</div></div>";
	}

	public function perstest()
	{
		if ($this->dbmodel->exists_mbti($this->session->userdata('id')))
		{
			redirect('profiel');
		}
		$this->load_vragen($data);
		$data['title'] = "Persoonlijkheidstest";
		$this->load->view('header', $data);
		$this->load->view('perstest', $data);
		$this->load->view('footer');
	}

	// Vertoont de resultaten van de persoonlijkheidstest
	public function resultaat()
	{
		$data['title'] = "Test resultaat";
		$resultaat = $this->dbmodel->get_mbti($this->session->userdata('id'));
		$E = $resultaat->ei;
		$I = 100 - $resultaat->ei;
		$N = $resultaat->ns;
		$S = 100 - $resultaat->ns;
		$T = $resultaat->tf;
		$F = 100 - $resultaat->tf;
		$J = $resultaat->jp;
		$P = 100 - $resultaat->jp;
		$mbti = $resultaat->mbti;
		$testmbti = str_split($mbti);
		$data['testresultaat'] = "Je bent " .strtoupper($testmbti[0] . "[" . $this->mbti_result($E, $I) . "&#37;] " . 
			$testmbti[1] . "[" . $this->mbti_result($N, $S) . "&#37;] " . 
			$testmbti[2] . "[" . $this->mbti_result($T, $F) . "&#37;] " . 
			$testmbti[3] . "[" . $this->mbti_result($J, $P) . "&#37;]");
		$this->load->view('header', $data);
		$this->load->view('persresultaat', $data);
		$this->load->view('footer');
	}

	// Laadt de vragen voor de persoonlijkheidstest uit een xml bestand
	private function load_vragen(&$data)
	{
		$vragen = simplexml_load_file("./assets/vragen.xml");
		$data['vragenlijst'] = form_open('profiel/perstest_form', 'id="vragenform"');
		$data['vragenlijst'] .= "<div>";
		foreach ($vragen->vraag as $vraag) 
		{	
			$vraagnummer = $vraag->naam;
			$vraagnaam = explode("_", $vraagnummer);
			$vraagnaam[0] = ucfirst($vraagnaam[0]);
			$vraagnum = $vraagnaam[0] . " " . $vraagnaam[1];
			$type = $vraag->type;
			$data['vragenlijst'] .= "<div class='vraag'><h1>$vraagnum</h1>";
			$data['vragenlijst'] .= "<input type='hidden' name=$vraagnummer value=$type />";
			foreach ($vraag->antwoord as $antwoord)
			{	
				$data['vragenlijst'] .= "<div class='antwoord'>";
				$antwoordtxt = $antwoord->text;
				$antwoordval = $antwoord->value;
				$data['vragenlijst'] .= "<input type='radio' name=$vraagnummer" . '_antwoord' . " " . "value=$antwoordval />";
				$data['vragenlijst'] .= "<label>$antwoordtxt</label>";
				$data['vragenlijst'] .= "</div>";
			}
			$data['vragenlijst'] .= "</div>";
		}
		$data['vragenlijst'] .= "<input type='submit' value='Submit test' />";
		$data['vragenlijst'] .= "</div>";
		$data['vragenlijst'] .= "</form>";
	}

	// Verwerkt de resultaten van de persoonlijkheidstest
	public function perstest_form()
	{
		$EI = 50;
		$NS = 50;
		$TF = 50;
		$JP = 50;
		for ($i = 0; $i <= 19; $i++)
		{
			switch ($this->input->post('vraag_' . $i)) {
				case 'EI':
				$this->mbti_change($EI, $this->input->post('vraag_' . $i . '_antwoord'), 10);
					break;
				case 'NS':
				$this->mbti_change($NS, $this->input->post('vraag_' . $i . '_antwoord'), 12.5);
					break;
				case 'TF':
				$this->mbti_change($TF, $this->input->post('vraag_' . $i . '_antwoord'), 12.5);
					break;
				case 'JP':
				$this->mbti_change($JP, $this->input->post('vraag_' . $i . '_antwoord'), (100/12));
					break;
				default:
					break;
			}
		}

		if ($this->dbmodel->exists_mbti($this->session->userdata('id')))
		{
			$this->dbmodel->update_mbti($this->session->userdata('id'), $EI, $NS, $TF, $JP);
		}
		else
		{
			$this->dbmodel->insert_mbti($this->session->userdata('id'), $EI, $NS, $TF, $JP);
		}

		$this->session->set_userdata(array(
			'EI' => $EI,
			'NS' => $NS,
			'TF' => $TF,
			'JP' => $JP
		));

		redirect('/profiel/resultaat');
	}

	private function mbti_change(&$mbti_entry, $val, $change)
	{
		switch ($val) {
			case '1':
			$mbti_entry += $change;
				break;
			case '2':
			$mbti_entry -= $change;
				break;
			case '3':
			$mbti_entry = $mbti_entry;
				break;
		}
	}

	private function mbti_result($val1, $val2){
		if ($val1 > $val2){
			return $val1;
		}
		else
		{
			return $val2;
		}
	}
}