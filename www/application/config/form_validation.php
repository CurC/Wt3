<?php

$config = array(
         'registratie' => array(
               array(
                     'field'   => 'roepnaam',
                     'label'   => 'Roepnaam',
                     'rules'   => 'trim|required|alpha_numeric_spaces'
                  ),
               array(
                     'field'   => 'naam',
                     'label'   => 'Naam',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'emailregistratie',
                     'label'   => 'Email',
                     'rules'   => 'trim|required|valid_email|is_unique[gebruiker.email]'
                  ),
               array(
                     'field'   => 'pass',
                     'label'   => 'Wachtwoord',
                     'rules'   => 'trim|required|min_length[6]'
                  ),
               array(
                     'field'   => 'passconf',
                     'label'   => 'Wachtwoord verificatie',
                     'rules'   => 'trim|matches[pass]'
                  ),
               array(
                     'field'   => 'beschrijving',
                     'label'   => 'Beschrijving',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'gebdatumm',
                     'label'   => 'Maand van geboorte',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'gebdatumy',
                     'label'   => 'Jaar van geboorte',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'gebdatumd',
                     'label'   => 'Dag van geboorte',
                     'rules'   => 'trim|required|date_valid|ouder_dan_18'
                  ),
               array(
                     'field'   => 'merken[]',
                     'label'   => 'Merken',
                     'rules'   => 'trim|required'
                  )
               ),
         'login' => array(
               array(
                     'field'   => 'email',
                     'label'   => 'Email',
                     'rules'   => 'trim|required|valid_email'
                  ),
               array(
                     'field'   => 'pass',
                     'label'   => 'Wachtwoord',
                     'rules'   => 'trim|required|min_length[6]'
                  )
            ),
         'edit_profiel' => array(
               array(
                     'field'   => 'roepnaam',
                     'label'   => 'Roepnaam',
                     'rules'   => 'trim|required|alpha_numeric_spaces'
                  ),
               array(
                     'field'   => 'naam',
                     'label'   => 'Naam',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'emailregistratie',
                     'label'   => 'Email',
                     'rules'   => 'trim|required|valid_email'
                  ),
               array(
                     'field'   => 'beschrijving',
                     'label'   => 'Beschrijving',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'gebdatumm',
                     'label'   => 'Maand van geboorte',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'gebdatumy',
                     'label'   => 'Jaar van geboorte',
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'gebdatumd',
                     'label'   => 'Dag van geboorte',
                     'rules'   => 'trim|required|date_valid'
                  )
            ),
         'edit_wachtwoord' => array(
               array(
                     'field'   => 'pass',
                     'label'   => 'Wachtwoord',
                     'rules'   => 'trim|required|min_length[6]'
                  ),
               array(
                     'field'   => 'passconf',
                     'label'   => 'Wachtwoord verificatie',
                     'rules'   => 'trim|matches[pass]'
                  )
            ),
         'zoek' => array(
               array(
                     'field'   => 'EI',
                     'label'   => 'Extrovert of Introvert',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'NS',
                     'label'   => 'Intuition of Sensing',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'TF',
                     'label'   => 'Thinking of Feeling',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'JP',
                     'label'   => 'Judging of Perceiving',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'geslachtsvoorkeur',
                     'label'   => 'Geslachtsvoorkeur',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'leeftijdsvoorkeur',
                     'label'   => 'Leeftijdsvoorkeur',
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'merken[]',
                     'label'   => 'Merkvoorkeur',
                     'rules'   => 'trim|required'
                  )
            )
         );