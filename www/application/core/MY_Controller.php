<?php

class MY_Controller extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
	}

	// Kijkt of alles opgestuurd moet worden of alleen de resultaten bij een ajax request
	protected function is_ajax_request(&$data, $view)
	{
		if ($this->input->is_ajax_request())
		{
			$this->load->view($view, $data);
		}
		else
		{
			$this->load->view('header', $data);
			$this->load->view($view, $data);
			$this->load->view('footer');
		}
	}

	// Set de foto als silllhouette als er geen foto is gegeven of als de gebruiker niet is ingelogd
	protected function set_sillhouette(&$foto, $geslacht)
	{
		$silhouette = $geslacht === 'M' ? 'man.png' : 'vrouw.png';
		$foto = $foto && $this->session->userdata('logged_in') ? $foto : $silhouette;
	}

	// Merkvoorkeur verwerken 
	protected function verwerk_merken_info(&$data, $merkvoorkeur)
	{
		$data['merkvoorkeur'] = array();
		$merken = explode(' ', $merkvoorkeur);
		foreach ($merken as $mid) {
			$merknaam = $this->dbmodel->get_merk_by_mid($mid);
			array_push($data['merkvoorkeur'], $merknaam);
		}
	}

	// Geeft alle gebruikers een relatietype
	protected function verwerk_relatietype(&$row)
	{
		$likes = $this->dbmodel->get_like_all($this->session->userdata('id'));
		$row['relatietype'] = 0;

		foreach($likes as $like)
		{
			if ($row['id'] === $this->session->userdata('id'))
			{
				return;
			}

			if ($row['id'] === $like->id2)
			{
				$row['relatietype'] = 1;
			}

			if ($row['id'] === $like->id)
			{
				$row['relatietype'] = 2;
			}
		}


		if ($this->dbmodel->exists_like_wz($this->session->userdata('id'), $row['id']))
		{
			$row['relatietype'] = 3;
		}
	}
}

class MY_AUTH extends MY_Controller
{

	  public function __construct()
	  {
		parent::__construct();

		if (!$this->session->userdata('logged_in'))
		{
			redirect('/login');
		}
	}
}