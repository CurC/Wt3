<?php

class Database_model extends CI_model
{

	//-------------------------------------------------------------------------------
	// Gebruikers
	//-------------------------------------------------------------------------------
	public function insert_gebruiker($roepnaam, $naam, $email, $wachtwoord, $geslacht, $geslachtsvoorkeur, $gebdatum, $leeftijdsvoorkeur, $beschrijving)
	{
		$data = array(
			'roepnaam' => $roepnaam,
			'naam' => $naam,
			'email' => $email,
			'wachtwoord' => $wachtwoord,
			'geslacht' => $geslacht,
			'geslachtvk' => $geslachtsvoorkeur,
			'geboortedatum' => $gebdatum,
			'leeftijdsvk' => $leeftijdsvoorkeur,
			'beschrijving' => $beschrijving
		);

		$this->db->insert('gebruiker', $data);
	}

	public function get_gebruiker($email, $wachtwoord)
	{
		$query = $this->db->get_where('gebruiker', array(
			'email' => $email, 
			'wachtwoord' => $wachtwoord
		));

		return $query->row();
	}

	public function get_gebruiker_random()
	{
		$this->db->limit(6);
		$this->db->order_by('random()');
		$query = $this->db->get('gebruiker');
		return $query->result_array();
	}

	public function get_gebruiker_browse()
	{
		$query = $this->db->get('gebruiker');
		return $query->result();
	}

	public function get_gebruiker_info($id)
	{
		$query = $this->db->get_where('gebruiker', array(
			'id' => $id
		));

		return $query->row();	
	}

	public function get_gebruiker_info_array($id)
	{
		$query = $this->db->get_where('gebruiker', array(
			'id' => $id
		));

		return $query->row_array();
	}

	public function update_gebruikersprofiel($id, $roepnaam, $naam, $email, $geslacht, $geslachtsvoorkeur, $gebdatum, $leeftijdsvoorkeur, $beschrijving)
	{
		$data = array(
			'roepnaam' => $roepnaam,
			'naam' => $naam,
			'email' => $email,
			'geslacht' => $geslacht,
			'geslachtvk' => $geslachtsvoorkeur,
			'geboortedatum' => $gebdatum,
			'leeftijdsvk' => $leeftijdsvoorkeur,
			'beschrijving' => $beschrijving
		);

		$this->db->where('id', $id);
		$this->db->update('gebruiker', $data);
	}

	public function update_gebruikerswachtwoord($id, $wachtwoord)
	{
		$this->db->set('wachtwoord', $wachtwoord);
		$this->db->where('id', $id);
		$this->db->update('gebruiker');
	}

	public function update_gebruikersfoto($id, $foto)
	{
		$this->db->set('foto', $foto);
		$this->db->where('id', $id);
		$this->db->update('gebruiker');		
	}

	public function update_merkvoorkeur($id, $merken)
	{
		$this->db->set('merkvk', $merken);
		$this->db->where('id', $id);
		$this->db->update('gebruiker');	
	}

	public function exists_email($email)
	{
		$query = $this->db->get_where('gebruiker', array(
			'email' => $email
		));

		return count($query->result()) === 1;
	}

	public function get_admin($id)
	{
		$this->db->select('admin');
		$query = $this->db->get_where('gebruiker', array(
			'id' => $id
		));

		return $query->row() === 1;
	}

	public function delete_gebruiker($id)
	{
		$this->db->delete('gebruiker', array('id' => $id));
	}

	//-------------------------------------------------------------------------------
	// MBTI
	//-------------------------------------------------------------------------------
	public function insert_mbti($id, $EI, $NS, $TF, $JP)
	{
		$data = array(
			'id' => $id,
			'ei' => $EI,
			'ns' => $NS,
			'tf' => $TF,
			'jp' => $JP
		);

		$this->db->insert('gebruiker_mbti', $data);
	}

	public function get_mbti($id)
	{
		$query = $this->db->get_where('gebruiker_mbti', array(
			'id' => $id
		));

		return $query->row();
	}

	public function exists_mbti($id)
	{
		$query = $this->db->get_where('gebruiker_mbti', array(
			'id' => $id
		));

		return count($query->result()) === 1;
	}

	public function update_mbti($id, $EI, $NS, $TF, $JP)
	{
		$data = array(
			'ei' => $EI,
			'ns' => $NS,
			'tf' => $TF,
			'jp' => $JP
		);

		$this->db->where('id', $id);
		$this->db->update('gebruiker_mbti', $data);
	}

	public function update_mbti_voorkeur($id, $vk_EI, $vk_NS, $vk_TF, $vk_JP)
	{
		$data = array(
			'vk_ei' => $vk_EI,
			'vk_ns' => $vk_NS,
			'vk_tf' => $vk_TF,
			'vk_jp' => $vk_JP
		);

		$this->db->where('id', $id);
		$this->db->update('gebruiker_mbti', $data);
	}

	public function get_mbti_browse()
	{
		$this->db->select('gebruiker.id, roepnaam, geslacht, geboortedatum, foto, beschrijving, geslachtvk, leeftijdsvk, merkvk, 
					ei, ns, tf, jp, vk_ei, vk_ns, vk_tf, vk_jp');
		$this->db->from('gebruiker');
		$this->db->join('gebruiker_mbti', 'gebruiker.id = gebruiker_mbti.id', 'natural');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function delete_mbti($id)
	{
		$this->db->delete('gebruiker_mbti', array('id' => $id));
	}

	//-------------------------------------------------------------------------------
	// Merken
	//-------------------------------------------------------------------------------
	public function get_merken()
	{
		$query = $this->db->get('merk');
		return $query->result();
	}

	public function get_merk_by_mid($mid)
	{
		$this->db->where('mid', $mid);
		$query = $this->db->get('merk');
		return $query->row();	
	}

	public function insert_merkvoorkeur($id, $mid)
	{
		$this->db->set('id', $id);
		$this->db->set('mid', $mid);
		$this->db->insert('merkvoorkeur');
	}

	public function delete_merkvoorkeur($id)
	{
		$this->db->delete('merkvoorkeur', array('id' => $id));
	}

	//-------------------------------------------------------------------------------
	// Likes
	//-------------------------------------------------------------------------------
	public function exists_like($id, $id2)
	{
		$this->db->where('id', $id);
		$this->db->where('id2', $id2);
		$query = $this->db->get('like');
		return count($query->result()) === 1;
	}

	public function exists_like_wz($id, $id2)
	{
		$this->db->where('id', $id);
		$this->db->where('id2', $id2);
		$this->db->or_where('id', $id2);
		$this->db->where('id2', $id);
		$query = $this->db->get('like');
		return count($query->result()) === 2;
	}

	public function get_like_all($id)
	{
		$this->db->where('id', $id);
		$this->db->or_where('id2', $id);
		$query = $this->db->get('like');
		return $query->result();
	}

	public function insert_like($id, $id2)
	{
		$this->db->set('id', $id);
		$this->db->set('id2', $id2);
		$this->db->insert('like');
	}

	public function delete_likes($id)
	{
		$this->db->delete('like', array('id' => $id));
	}

	//-------------------------------------------------------------------------------
	// Admin
	//-------------------------------------------------------------------------------
	public function get_adminvar($constante)
	{
		$query = $this->db->get_where('adminvar', array(
			'constante' => $constante
		));

		return $query->row()->waarde;
	}

	public function update_adminvar($constante, $waarde)
	{
		$this->db->set('waarde', $waarde);
		$this->db->where('constante', $constante);
		$this->db->update('adminvar');
	}

}