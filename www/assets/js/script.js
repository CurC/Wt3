$(document).ready(function()
{
	function shuffleVragen()
	{
		var cards = $(".vraag");
		for(var i = 0; i < cards.length; i++)
		{
			var target = Math.floor(Math.random() * cards.length -1) + 1;
			var target2 = Math.floor(Math.random() * cards.length -1) +1;
			cards.eq(target).before(cards.eq(target2));
		}
	}

	// Shufflet alle vragen en nummert ze
	$('#perscontent').find('#vragenform').each(function () {
		shuffleVragen();
		$(this).find('.vraag > h1').each(function(index) {
			$(this).text('Vraag ' + (index + 1));
		});
	});

	// Zet de merkenkeuze in een accordian widget
	$('#merkregistratie').accordion({
		active: false,
		collapsible: true
	});


	// Navigatie menu animaties
	$("li.navkop").click(function () {
		$(this).siblings("li.navhoofdkop2").stop().slideDown('medium');
	});

	$('li.hidden').hide();

	$("li.navhoofdkop > ul").mouseleave(function () {
		$(this).find("li.navhoofdkop2").stop().slideUp('medium', function () {
			$(this).css('color', '#000');
		});
	});

	$("li.navkop, li.navhoofdkop2").hover(function () {
		$(this).animate({
			color: '#777'
		}, 200);
	}, function () {
		$(this).animate({
			color: '#000'
		}, 200);
	});

	// Browse ajax request
	$('body').on('click', '.page', function(event)
	{
		$.ajaxPrefilter(function(options, originalOptions, jqXHR ) {
			options.async = true;
		});

		$('#content').load($(this).attr('href'));
		return false;
	});

	$('body').on('click', '#meerrandom', function(event)
	{
		$.ajaxPrefilter(function(options, originalOptions, jqXHR ) {
			options.async = true;
		});

		$('#content').load($(this).attr('href'));
		return false;
	});

	// Delete button
	$("#delete-confirm").dialog({
		autoOpen: false,
		modal: true,
		buttons: {
			"Confirm delete": function () {
				window.location.replace($('#deletelink').attr('value'));
			},
			Cancel: function () {
				$(this).dialog("close");
			}
		}
	});

	$('#delete-button').click(function () {
		$('#delete-confirm').dialog("open");
	});

	// Leeftijd range slider
	$("#leeftijdslider").slider({
	  range: true,
	  min: 18,
	  max: 100,
	  values: [18, 25],
	  slide: function( event, ui ) {
		$("#leeftijd").val(ui.values[0] + "-" + ui.values[1]);
	  }
	});
	$("#leeftijd").val($("#leeftijdslider").slider("values", 0) +
		"-" + $("#leeftijdslider").slider("values", 1));

	// Admin slider
	$('#alfaslider').slider({
		min: 0,
		max: 1,
		value: $('#alfaval').val(),
		step: 0.01,
		slide: function(event, ui) {
			$('#alfa').val(ui.value);
		}
	});
	$("#alfa").val($("#alfaslider").slider("value"));

	$('#xfactorslider').slider({
		min: 0,
		max: 1,
		value: $('#xfactorval').val(),
		step: 0.01,
		slide: function(event, ui) {
			$('#xfactor').val(ui.value);
		}
	});
	$("#xfactor").val($("#xfactorslider").slider("value"));
});
